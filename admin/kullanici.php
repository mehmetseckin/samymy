<?php
session_start();
$_SESSION['title'] = 'Kullanıcılar..';
include '../lib.php';
include '../veritabani.php';
if(!isset($_SESSION['yetki']) || $_SESSION['yetki'] != PERMISSION_ADMIN) {
	redirect('login.php');
	exit;
}
?>
<?php include "ust.php"; ?>
<?php include "menu.php"; ?>

<?php
if(isset($_GET["islem"])) $islem = $_GET["islem"]; else $islem = "listele";
if($islem == "ekle") { 
	if(isset($_POST["submit"])) {
                $ad = $_POST["ad"];
                $soyad = $_POST["soyad"];
                $email = $_POST["email"];
                $telefon = $_POST["telefon"];
                $sifre = $_POST["sifre"];
                $adres = $_POST["adres"];
    
	
		$hata = "";
		$sorgu = "SELECT email FROM kullanicilar WHERE email='$email'";
		$sonuc = mysql_query($sorgu);
		if(mysql_num_rows($sonuc) > 0) {
                    $hata = "Bu email zaten kayıtlı.";
                }
		if($hata == "") {
                    $sql = "INSERT INTO kullanicilar(ad, soyad, telefon, email, sifre, adres) VALUES('$ad', '$soyad', '$telefon', '$email', '$telefon', '$adres');";
                    $success = mysql_query($sql);
                    if($success) {
                        message("$ad $soyad için kayıt işleminiz başarıyla tamamlandı.", "success");
                    }
                    else {
                        message("Bir hata oluştu, lütfen tekrar deneyiniz.");
                    }
		} else {
			message($hata, "error");
		}
	}
?>
	<div id="kullanici-ekle-form">
	<form name="kullanici-ekle" method="post" action="kullanici.php?islem=ekle">
	<fieldset>
                <table>
                <tr><td>Adınız</td><td>: <input type="text" name="ad" ></td></tr>
                <tr><td>Soyadınız</td><td>: <input type="text" name="soyad" ></td></tr>
                <tr><td>E-Mail Adresiniz</td><td>: <input type="text" name="email" ></td></tr>
                <tr><td>Telefon Numaranız</td><td>: <input type="text" name="telefon" ></td></tr>
                <tr><td>Şifreniz</td><td>: <input type="text" name="sifre" ></td></tr>
                <tr><td style="vertical-align:top; margin-top: 0px;">Adres</td><td>   <textarea name="adres" id="adres" cols="25" rows="5"> </textarea></td></tr>
                <tr><td colspan=2><input type="submit" name="submit" value="Ekle"></td></tr>
                </table>
	</fieldset>
	</form>
	</div>
<?php	
}
if($islem == "duzenle") {
	if(isset($_POST["submit"])) {
        $ad = $_POST["ad"];
        $soyad = $_POST["soyad"];
        $email = $_POST["email"];
        $telefon = $_POST["telefon"];
        $sifre = $_POST["sifre"];
        $adres = $_POST["adres"];
        $id = $_POST["id"];
	 $sorgu = "UPDATE kullanicilar SET ad='$ad', soyad='$soyad', email='$email', telefon='$telefon', sifre='$sifre', adres='$adres' WHERE id=$id LIMIT 1;";
	 $sonuc = mysql_query($sorgu);
	 if(!$sonuc)
		message("Kullanıcı düzenlenemedi!");
	else
		message("Kullanıcı başarıyla düzenlendi!", "success");
	}
	if(isset($_GET["id"])) $id = $_GET["id"]; else $id = -1;

	echo '<div id="kategoriler">';
        if($id > 0) {
	$sorgu = "SELECT * FROM kullanicilar WHERE id='$id' LIMIT 1;";
	$sonuc = mysql_query($sorgu);            
	echo '<fieldset><table>';
	echo '<form name="kategori-duzenle" action="kullanici.php?islem=duzenle" method="post">';
	while($kullanici = mysql_fetch_array($sonuc)) {
?>
                <tr><td>Adınız</td><td>: <input type="text" name="ad" value="<?=$kullanici["ad"]?>"></td></tr>
                <tr><td>Soyadınız</td><td>: <input type="text" name="soyad" value="<?=$kullanici["soyad"]?>"></td></tr>
                <tr><td>E-Mail Adresiniz</td><td>: <input type="text" name="email" value="<?=$kullanici["email"]?>"></td></tr>
                <tr><td>Telefon Numaranız</td><td>: <input type="text" name="telefon" value="<?=$kullanici["telefon"]?>"></td></tr>
                <tr><td>Şifreniz</td><td>: <input type="text" name="sifre" value="<?=$kullanici["sifre"]?>" ></td></tr>
                <tr><td style="vertical-align:top; margin-top: 0px;">Adres</td><td>   <textarea name="adres" id="adres" cols="25" rows="5"><?=$kullanici["adres"]?></textarea></td></tr>
                <tr><td colspan=2><input type="submit" name="submit" value="Kaydet"></td></tr>
                <input type="hidden" name="id" value="<?=$kullanici["id"]?>">
<?php
	}
	echo '</fieldset></form>';
	echo '</table>';
        }
        
            $sorgu = "SELECT * FROM kullanicilar";
            $sonuc = mysql_query($sorgu);    
            echo '<ul>';
            while($kullanici = mysql_fetch_array($sonuc)) {
                echo '<li>'.$kullanici["ad"].' '.$kullanici["soyad"].', '.$kullanici["email"].' <a href="kullanici.php?islem=duzenle&id='.$kullanici["id"].'">Düzenle</a> <a href="kullanici.php?islem=sil&id='.$kullanici["id"].'">Sil</a></li>';
            }
	echo '</div>';
}
if($islem == "sil") {
	if(isset($_GET["id"])) $id = $_GET["id"]; else $id = -1;
        if($id > 0) {
            $sorgu = "DELETE FROM kullanicilar WHERE id='$id' LIMIT 1";
            $sonuc = mysql_query($sorgu);
            if(!$sonuc)
                    message("Kullanıcı silinemedi!");
            else
                    message("Kullanıcı başarıyla silindi!", "success");
            }
        }

?>

<?php include "alt.php"; ?>